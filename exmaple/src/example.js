const redux = require('redux');
const reduxLink = require('../../index.js');

function exampleBoxText(state = '', action) {
  switch (action.type) {
    case 'UPDATE_BOX_TEXT':
      return action.payload;
    default:
      return state;
  }
}

function exampleBoxStyle(state = '', action) {
  switch (action.type) {
    case 'UPDATE_BOX_STYLE':
      return action.payload;
    default:
      return state;
  }
}


const reducers = redux.combineReducers({
  exampleBoxText: exampleBoxText,
  exampleBoxStyle: exampleBoxStyle,
});
const store = reduxLink(redux.createStore(reducers));
const exampleBox = document.querySelector('.example-box');

store.linkText(exampleBox, 'exampleBoxText');
store.linkStyle(exampleBox, 'exampleBoxStyle');

store.dispatch({
  type: 'UPDATE_BOX_TEXT',
  payload: 'Hello World!',
});
store.dispatch({
  type: 'UPDATE_BOX_STYLE',
  payload: 'color:red;',
});

