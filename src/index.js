const _isPlainObject = require('lodash/isPlainObject');
const _isArray = require('lodash/isArray');
const _set = require('lodash/set');

// If reudx is using combineReducers() we need to find the reducer
// we want by it's key
function linkCombinedReducers(store, linkProperty, node, sourceKey) {
  var previousSourceValue = store.getState()[sourceKey];
  
  store.subscribe(function () {
    var currentSourceValue = store.getState()[sourceKey];

    // Don't update DOM if vale in redux has not changed
    if (currentSourceValue && currentSourceValue !== previousSourceValue) {
      if(!_isArray(linkProperty)) {
        node[linkProperty] = currentSourceValue;
      } else {
        _set(node, linkProperty, currentSourceValue);
      }
      previousSourceValue = currentSourceValue;
    }
  });
}

// If redux is not using combineReducers() then we don't need
// to worry about keys
function linkPlainReducer(store, linkProperty, node) {
  var previousSourceValue = store.getState();
  
  store.subscribe(function () {
    var currentSourceValue = store.getState();

    // Don't update DOM if vale in redux has not changed
    if (currentSourceValue && currentSourceValue !== previousSourceValue) {
      if(!_isArray(linkProperty)) {
        node[linkProperty] = currentSourceValue;
      } else {
        _set(node, linkProperty, currentSourceValue);
      }
      previousSourceValue = currentSourceValue;
    }
  });
}

function link(store, linkProperty, node, sourceKey) {
  var initialState = store.getState();

  if(!linkProperty) {
    console.error('reduxLink: No "linkProperty" provided.');
    return;
  }
  
  if(!store) {
    console.error('reduxLink: No "store" provided.');
    return;
  }

  if(!node) {
    console.error('reduxLink: No "node" provided.');
    return;
  }

  if(!sourceKey && _isPlainObject(initialState)) {
    console.error('reduxLink: state is an object but no "sourceKey" provided.');
    return;
  }

  if(sourceKey && !_isPlainObject(initialState)) {
    console.error('reduxLink: state is not an object but a "sourceKey" was provided.');
    return;
  }

  if(_isPlainObject(initialState)) {
    linkCombinedReducers(linkProperty, store, node, sourceKey);
  } else {
    linkPlainReducer(linkProperty, store, node);
  }
}


function reduxLink(store) {
  store.linkHTML = link.bind(null, store, 'innerHTML');
  store.linkText = link.bind(null, store, 'textContent');
  store.linkClass = link.bind(null, store, 'classname');
  store.linkStyle = link.bind(null, store, ['style', 'cssText']);
  store.link = link.bind(null, store);
  
  return store;
}

module.exports = reduxLink;
