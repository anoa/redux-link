# Redux-link
Links values from a redux store to the DOM.

## API

Pass a noraml redux store to the redux-link function which returns
a linked store.

```javascript
    const linkedStore = reduxLink(normalStore);
```

You can now call `link()` functions on this store. A link function takes the
following parameters:

- (DOM Node) node to witch the reducer will be mapped
- (String) name of the reducer from wich the value should be mapped to the DOM node
- (String) property on the DOM node that will be updated with the value of the second parameter. if what you want to update is not directry on the node give an array that represents a path to the desired property.

There are also some convenience methods that let you skip specifying the property on the node you want to update.

- `linkText()` property to update will be set as `textContent`
- `linkClass()` property to update will be set as `classname`
- `linkClass()` property to update will be set as `innerHTML`
- `linkStyle()` property to update will be set as `cssText` on the `style` property of the node

## Example
````javascript
    const redux = require('redux');
    const reduxLink = require('redux-link');

    function exampleBoxText(state = '', action) {
        switch (action.type) {
            case 'UPDATE_BOX_TEXT':
                return action.payload;
            default:
                return state;
        }
    }

    function exampleBoxStyle(state = '', action) {
        switch (action.type) {
            case 'UPDATE_BOX_STYLE':
                return action.payload;
            default:
                return state;
        }
    }


    const reducers = redux.combineReducers({
        exampleBoxText: exampleBoxText,
        exampleBoxStyle: exampleBoxStyle,
    });
    const store = reduxLink(redux.createStore(reducers));
    const exampleBox = document.querySelector('.example-box');

    store.linkText(exampleBox, 'exampleBoxText');
    store.linkStyle(exampleBox, 'exampleBoxStyle');

    store.dispatch({
        type: 'UPDATE_BOX_TEXT',
        payload: 'Hello World!',
    });
    store.dispatch({
        type: 'UPDATE_BOX_STYLE',
        payload: 'color:red;',
    });

    // exampleBox now has the textContent: 'Hello World!'
    // and an inline style: style="color:red;"
````


